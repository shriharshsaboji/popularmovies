package com.popularmovies.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.popularmovies.R;
import com.popularmovies.ui.movielisting.MovieListingActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        startSplash();

    }

    private void startSplash() {
        int splash_timer = 1000;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //start feeds activity
                Intent feedsActivityIntent = new Intent(SplashActivity.this, MovieListingActivity.class);
                startActivity(feedsActivityIntent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                //Finish the activity
                finish();

            }
        }, splash_timer);

    }
}
