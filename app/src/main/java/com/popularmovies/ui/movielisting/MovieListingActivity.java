package com.popularmovies.ui.movielisting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.popularmovies.Constants;
import com.popularmovies.R;
import com.popularmovies.model.Movie;
import com.popularmovies.network.NetworkUtils;
import com.popularmovies.ui.moviedetail.MovieDetailActivity;
import com.popularmovies.ui.movielisting.adapter.MovieListingAdapter;
import com.popularmovies.utils.GridSpacingItemDecoration;
import com.popularmovies.utils.PreferenceUtil;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class MovieListingActivity extends AppCompatActivity implements MovieListingContract.ListingView, MovieListingAdapter.ItemClickListener {

    private static final String TAG = "MovieListingActivity";

    int columns = 2;
    private MovieListingPresenter movieListingPresenter;
    private RecyclerView mRecyclerViewMoviesList;
    private MovieListingAdapter mMovieListingAdapter;
    private RelativeLayout mRelativeLayout;
    private LinearLayout mNoInternetLinearLayout;
    private ShimmerRecyclerView mShimmerRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_listing);

        initializePresenter();

        //Set view in the presenter
        movieListingPresenter.setView(this);

        initializeViews();

        if (PreferenceUtil.getInstance().getBooleanValue(Constants.IS_FIRST_LAUNCH, true)
                && !NetworkUtils.isNetworkConnected(this)) {
            //For first time it is mandatory to connect to internet
            mNoInternetLinearLayout.setVisibility(View.VISIBLE);
        } else {
            PreferenceUtil.getInstance().setBooleanValue(Constants.IS_FIRST_LAUNCH, false);

            mNoInternetLinearLayout.setVisibility(View.GONE);
            mRecyclerViewMoviesList.setVisibility(View.VISIBLE);
            mShimmerRecyclerView.setVisibility(View.VISIBLE);
            mShimmerRecyclerView.showShimmerAdapter();

            //initiate call from  view to presenter
            movieListingPresenter.fetchPopularMovies();
        }
    }

    private void initializeViews() {
        mRelativeLayout = findViewById(R.id.rl_main);
        mRecyclerViewMoviesList = findViewById(R.id.rv_movie_list);
        mNoInternetLinearLayout = findViewById(R.id.ll_no_internet);
        mShimmerRecyclerView = findViewById(R.id.shimmer_recycler_view);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, columns);
        mRecyclerViewMoviesList.setLayoutManager(gridLayoutManager);

        int spacing = 30; // 30px its in pixels
        boolean includeEdge = true;
        mRecyclerViewMoviesList.addItemDecoration(new GridSpacingItemDecoration(columns, spacing, includeEdge));
        mShimmerRecyclerView.addItemDecoration(new GridSpacingItemDecoration(columns, spacing, includeEdge));

        mMovieListingAdapter = new MovieListingAdapter(this, this);

        mRecyclerViewMoviesList.setAdapter(new ScaleInAnimationAdapter(mMovieListingAdapter));
    }

    private void initializePresenter() {
        movieListingPresenter = new MovieListingPresenter();
    }

    @Override
    public void setDataToRecycler(ArrayList<Movie> movieArray) {
        if (mMovieListingAdapter != null) {
            mShimmerRecyclerView.hideShimmerAdapter();
            mRecyclerViewMoviesList.smoothScrollToPosition(0);
            mMovieListingAdapter.setData(movieArray);
        }
    }

    @Override
    public void showToast(String offlineString) {
        Toast.makeText(this, offlineString, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(int position, Movie movie, ImageView posterImageView) {
        Intent startDetailsActivity = new Intent(MovieListingActivity.this, MovieDetailActivity.class);
        startDetailsActivity.putExtra(Constants.EXTRA_KEY_MOVIE, movie);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, posterImageView, "posterTransition");
        startActivity(startDetailsActivity, options.toBundle());
    }
}
