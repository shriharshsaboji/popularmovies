package com.popularmovies.ui.movielisting;

import com.popularmovies.model.Movie;

import java.util.ArrayList;

/**
 * Created on 21/9/18.
 * shriharsh
 */
public class MovieListingContract {

    public interface ListingView {

        void setDataToRecycler(ArrayList<Movie> movieArray);

        void showToast(String offlineString);
    }

    public interface ListingPresenter {

        void setView(MovieListingActivity movieListingView);

        void fetchPopularMovies();
    }

}
