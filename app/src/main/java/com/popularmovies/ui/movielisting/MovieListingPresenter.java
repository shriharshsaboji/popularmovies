package com.popularmovies.ui.movielisting;

import android.util.Log;

import com.popularmovies.BuildConfig;
import com.popularmovies.R;
import com.popularmovies.db.MovieDatabase;
import com.popularmovies.model.Movie;
import com.popularmovies.model.MovieListResponse;
import com.popularmovies.network.NetworkUtils;
import com.popularmovies.network.RetrofitAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created on 21/9/18.
 * shriharsh
 */
public class MovieListingPresenter implements MovieListingContract.ListingPresenter {
    private static final String TAG = "MovieListingPresenter";
    private MovieListingActivity movieListingView;
    private ArrayList<Movie> movieArray = new ArrayList<>();


    @Override
    public void setView(MovieListingActivity movieListingView) {
        this.movieListingView = movieListingView;
    }

    @Override
    public void fetchPopularMovies() {
        String snackbarString = movieListingView.getString(R.string.fetching_movies);

        if (NetworkUtils.isNetworkConnected(movieListingView)) {
            movieListingView.showToast(snackbarString);
            getMoviesFromNetwork();
        } else {
            snackbarString = movieListingView.getString(R.string.no_network);

            List<Movie> moviesList = MovieDatabase.getInstance(movieListingView).getMoviesDao().getAllMovies();

            if (moviesList != null && moviesList.size() > 0) {
                snackbarString = movieListingView.getString(R.string.offline_data);
                //set data to recycler view
                movieListingView.setDataToRecycler((ArrayList<Movie>) moviesList);
            }

            movieListingView.showToast(snackbarString);
        }

    }

    private void getMoviesFromNetwork() {
        RetrofitAPI retrofitAPI = NetworkUtils.getRetrofit().create(RetrofitAPI.class);

        Call<MovieListResponse> call = retrofitAPI.getMovies("popular", BuildConfig.TMDB_API_KEY, "en-US", 1);

        call.enqueue(new Callback<MovieListResponse>() {
            @Override
            public void onResponse(Call<MovieListResponse> call, Response<MovieListResponse> response) {
                MovieListResponse movieListResponse = response.body();
                movieArray.clear();
                if (movieListResponse != null) {
                    movieArray.addAll(movieListResponse.getResults());

                    //add data to DB
                    MovieDatabase.getInstance(movieListingView).getMoviesDao().insert(movieArray);
                }

                List<Movie> moviesList = MovieDatabase.getInstance(movieListingView).getMoviesDao().getAllMovies();

                //set data to recycler view
                movieListingView.setDataToRecycler((ArrayList<Movie>) moviesList);
            }

            @Override
            public void onFailure(Call<MovieListResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

}
