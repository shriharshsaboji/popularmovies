package com.popularmovies.ui.movielisting.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.popularmovies.R;
import com.popularmovies.model.Movie;
import com.popularmovies.network.RetrofitAPI;

import java.util.ArrayList;

public class MovieListingAdapter extends RecyclerView.Adapter<MovieListingAdapter.MovieViewHolder> {

    private ArrayList<Movie> mMoviesArrayList;
    private Context mContext;
    private ItemClickListener mClickListener;

    public MovieListingAdapter(Context context, ItemClickListener itemClickListener) {
        this.mContext = context;
        this.mMoviesArrayList = new ArrayList<>();
        this.mClickListener = itemClickListener;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.rv_movie_item, viewGroup, false);
        return new MovieViewHolder(view);
    }

    public void setData(ArrayList<Movie> values) {
        if (values != null && values.size() > 0) {
            mMoviesArrayList.clear();
            mMoviesArrayList.addAll(values);
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        Movie theMovie = mMoviesArrayList.get(position);

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();

        Glide.with(mContext)
                .load(RetrofitAPI.POSTER_BASE_URL + theMovie.getPosterPath())
                .apply(options)
                .into(holder.mImageViewPoster);
    }

    @Override
    public int getItemCount() {
        if (mMoviesArrayList == null) return 0;
        else return mMoviesArrayList.size();
    }

    public interface ItemClickListener {
        void onItemClick(int position, Movie movie, ImageView posterImageView);
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mImageViewPoster;

        MovieViewHolder(View itemView) {
            super(itemView);

            mImageViewPoster = itemView.findViewById(R.id.iv_poster);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                int adapterPosition = getAdapterPosition();
                mClickListener.onItemClick(adapterPosition, mMoviesArrayList.get(adapterPosition), mImageViewPoster);
            }
        }
    }
}
