package com.popularmovies.ui.moviedetail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.popularmovies.R;
import com.popularmovies.model.Cast;
import com.popularmovies.network.RetrofitAPI;

import java.util.ArrayList;

public class CastListAdapter extends RecyclerView.Adapter<CastListAdapter.CastViewHolder> {
    private ArrayList<Cast> mCastList;
    private Context mContext;

    public CastListAdapter(Context context) {
        this.mContext = context;
        this.mCastList = new ArrayList<>();
    }

    public void setData(ArrayList<Cast> values) {
        if (values != null && values.size() > 0) {
            mCastList.clear();
            mCastList.addAll(values);
        }
        notifyDataSetChanged();
    }

    @Override
    public CastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.rv_cast_item, parent, false);
        return new CastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CastViewHolder holder, int position) {
        holder.mCastTextView.setText(mCastList.get(position).getName());
        Glide.with(mContext)
                .load(RetrofitAPI.POSTER_BASE_URL + mCastList.get(position).getProfilePath())
                .into(holder.mCastImageView);
    }

    @Override
    public int getItemCount() {
        return mCastList.size();
    }

    public class CastViewHolder extends RecyclerView.ViewHolder {
        public TextView mCastTextView;
        public ImageView mCastImageView;

        public CastViewHolder(View itemView) {
            super(itemView);
            mCastImageView = itemView.findViewById(R.id.iv_cast_avatar);
            mCastTextView = itemView.findViewById(R.id.tv_cast_name);
        }

    }
}
