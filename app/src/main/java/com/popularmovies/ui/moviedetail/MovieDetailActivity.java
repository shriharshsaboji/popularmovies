package com.popularmovies.ui.moviedetail;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.flexbox.FlexboxLayout;
import com.popularmovies.R;
import com.popularmovies.model.Cast;
import com.popularmovies.model.Genre;
import com.popularmovies.model.Movie;
import com.popularmovies.network.RetrofitAPI;
import com.popularmovies.ui.moviedetail.adapter.CastListAdapter;
import com.popularmovies.utils.DateUtil;

import java.util.ArrayList;
import java.util.List;

import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

/**
 * Created on 22/9/18.
 * shriharsh
 */
public class MovieDetailActivity extends AppCompatActivity implements MovieDetailContract.DetailView, View.OnClickListener {

    private AppBarLayout mAppBarLayout;
    private ImageView mAppBarBack;
    private ImageView mToolBarBack;
    private ImageView mAppBarPoster;
    private TextView mToolBarTitle;
    private TextView mAppBarTitle;
    private Toolbar mToolBar;
    private Movie mMovie;
    private TextView mMovieOverview;
    private TextView mVoterCount;
    private TextView mRating;
    private TextView mReleaseDate;
    private TextView mGenreLabelTextView;
    private FlexboxLayout mFlexboxLayoutTags;
    private RecyclerView mCastRecyclerView;
    private CastListAdapter mCastListAdapter;
    private LinearLayout mDirectorLinearLayout;
    private TextView mDirectorNameTextView;
    private TextView mCastLabelTextView;
    private MovieDetailPresenter movieDetailPresenter;
    private CoordinatorLayout mCoordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activty_movie_detail);

        initViews();

        initializePresenter();

        //Set view in the presenter
        movieDetailPresenter.setView(this);

        movieDetailPresenter.getDataFromBundle(getIntent().getExtras());


        mToolBar.setTitle("");
        mToolBar.setVisibility(View.INVISIBLE);
        setSupportActionBar(mToolBar);

        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;

                    mToolBar.setVisibility(View.VISIBLE);
                } else if (isShow) {
                    isShow = false;

                    mToolBar.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void initializePresenter() {
        movieDetailPresenter = new MovieDetailPresenter();
    }


    public void setDataToRelatedViews(Movie movie) {
        mMovie = movie;
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();

        Glide.with(this)
                .load(RetrofitAPI.BACKDROP_BASE_URL + this.mMovie.getBackdropPath())
                .apply(options)
                .into(mAppBarPoster);

        mToolBarTitle.setText(this.mMovie.getTitle());
        mAppBarTitle.setText(this.mMovie.getTitle());
        mMovieOverview.setText(this.mMovie.getOverview());
        mVoterCount.setText(Integer.toString(this.mMovie.getVoteCount()));
        mRating.setText(this.mMovie.getAverageVote());
        mReleaseDate.setText(DateUtil.changeDateFormat(this.mMovie.getReleaseDate(), "yyyy-MM-dd", "dd-MM-yy"));
    }

    @Override
    public void setGenres(List<Genre> genres) {
        mGenreLabelTextView.setVisibility(View.VISIBLE);
        ChipCloudConfig config = new ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.single)
                .checkedChipColor(getResources().getColor(R.color.colorLightGrey))
                .checkedTextColor(getResources().getColor(R.color.colorBlack))
                .uncheckedChipColor(getResources().getColor(R.color.colorLightGrey))
                .uncheckedTextColor(getResources().getColor(R.color.colorBlack));

        ChipCloud chipCloud = new ChipCloud(this, mFlexboxLayoutTags, config);

        for (Genre genre : genres) {
            chipCloud.addChip(genre.getName());
        }
    }

    private void initViews() {
        mAppBarLayout = findViewById(R.id.abl_movie_detail);
        mAppBarBack = findViewById(R.id.iv_back);
        mAppBarBack.setOnClickListener(this);
        mToolBarBack = findViewById(R.id.tb_iv_back);
        mToolBarBack.setOnClickListener(this);
        mAppBarPoster = findViewById(R.id.iv_poster);
        mToolBarTitle = findViewById(R.id.tb_movie_title);
        mToolBar = findViewById(R.id.tb_movie_detail);
        mAppBarTitle = findViewById(R.id.abl_movie_title);
        mMovieOverview = findViewById(R.id.tv_movie_overview);
        mVoterCount = findViewById(R.id.tv_voters_count);
        mRating = findViewById(R.id.tv_rating);
        mReleaseDate = findViewById(R.id.tv_release_date);
        mDirectorLinearLayout = findViewById(R.id.ll_director_layout);
        mDirectorNameTextView = findViewById(R.id.tv_director_name);
        mGenreLabelTextView = findViewById(R.id.tv_genre_label);
        mFlexboxLayoutTags = findViewById(R.id.fl_genre);
        mCastLabelTextView = findViewById(R.id.tv_cast_label);
        mCastRecyclerView = findViewById(R.id.rv_cast);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mCastRecyclerView.setLayoutManager(layoutManager);
        mCastListAdapter = new CastListAdapter(this);
        mCastRecyclerView.setAdapter(new ScaleInAnimationAdapter(mCastListAdapter));
        mCoordinatorLayout = findViewById(R.id.cl_movie_detail);
        mCastRecyclerView.setFocusable(false);
        mMovieOverview.requestFocus();
    }

    @Override
    public void showToast(String string) {
        Toast.makeText(this, string, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDirectorName(String name) {
        mDirectorLinearLayout.setVisibility(View.VISIBLE);
        mDirectorNameTextView.setText(name);
    }

    @Override
    public void showCastList(ArrayList<Cast> cast) {
        if (mCastListAdapter != null) {
            mCastLabelTextView.setVisibility(View.VISIBLE);
            mCastRecyclerView.smoothScrollToPosition(0);
            mCastListAdapter.setData(cast);
        }
    }

    @Override
    public void scrollNestedViewToTop() {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv_back:
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }
}
