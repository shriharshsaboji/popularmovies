package com.popularmovies.ui.moviedetail;

import android.os.Bundle;

import com.popularmovies.model.Cast;
import com.popularmovies.model.Genre;
import com.popularmovies.model.Movie;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 23/9/18.
 * shriharsh
 */
public interface MovieDetailContract {

    public interface DetailView {

        void setDataToRelatedViews(Movie movie);

        void setGenres(List<Genre> genres);

        void showDirectorName(String name);

        void showCastList(ArrayList<Cast> cast);

        void scrollNestedViewToTop();

        void showToast(String string);
    }

    public interface DetailPresenter {

        void setView(MovieDetailActivity movieDetailActivity);

        void getDataFromBundle(Bundle extras);
    }

}
