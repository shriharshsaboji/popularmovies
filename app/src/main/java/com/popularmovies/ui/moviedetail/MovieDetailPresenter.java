package com.popularmovies.ui.moviedetail;

import android.os.Bundle;

import com.popularmovies.BuildConfig;
import com.popularmovies.Constants;
import com.popularmovies.R;
import com.popularmovies.db.CastDao;
import com.popularmovies.db.CrewDao;
import com.popularmovies.db.GenreDao;
import com.popularmovies.db.MovieDatabase;
import com.popularmovies.model.Cast;
import com.popularmovies.model.CastCrewDetails;
import com.popularmovies.model.Crew;
import com.popularmovies.model.Genre;
import com.popularmovies.model.Movie;
import com.popularmovies.model.MovieDetails;
import com.popularmovies.network.NetworkUtils;
import com.popularmovies.network.RetrofitAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created on 23/9/18.
 * shriharsh
 */
public class MovieDetailPresenter implements MovieDetailContract.DetailPresenter {

    private static final String TAG = "MovieDetailPresenter";
    private MovieDetailActivity movieDetailActivity;
    private Movie mMovie;

    @Override
    public void setView(MovieDetailActivity movieDetailActivity) {
        this.movieDetailActivity = movieDetailActivity;
    }


    @Override
    public void getDataFromBundle(Bundle bundle) {
        if (bundle != null) {
            mMovie = (Movie) bundle.getSerializable(Constants.EXTRA_KEY_MOVIE);

            if (!NetworkUtils.isNetworkConnected(movieDetailActivity)) {
                movieDetailActivity.showToast(movieDetailActivity.getString(R.string.offline_data));
            }

            movieDetailActivity.setDataToRelatedViews(mMovie);

            fetchCastDetails();
            fetchGenreDetails();
        }
    }

    private void fetchGenreDetails() {
        if (NetworkUtils.isNetworkConnected(movieDetailActivity)) {
            RetrofitAPI retrofitAPI = NetworkUtils.getRetrofit().create(RetrofitAPI.class);
            Call<MovieDetails> movieDetailsCall = retrofitAPI.getDetails(mMovie.getId(), BuildConfig.TMDB_API_KEY, "en-US");
            movieDetailsCall.enqueue(new Callback<MovieDetails>() {
                @Override
                public void onResponse(Call<MovieDetails> call, Response<MovieDetails> response) {
                    if (response.body() != null) {
                        List<Genre> genres = response.body().getGenres();

                        GenreDao genreDao = MovieDatabase.getInstance(movieDetailActivity).getGenreDao();

                        for (Genre genre : genres) {
                            genreDao.insert(new Genre(genre.getName(), genre.getId(), mMovie.getId()));
                        }

                        movieDetailActivity.setGenres(genres);
                    }
                }

                @Override
                public void onFailure(Call<MovieDetails> call, Throwable t) {

                }
            });
        } else {
            GenreDao genreDao = MovieDatabase.getInstance(movieDetailActivity).getGenreDao();

            List<Genre> cachedGenre = genreDao.findGenresForMovie(mMovie.getId());

            if (cachedGenre != null && cachedGenre.size() > 0) {
                movieDetailActivity.setGenres(cachedGenre);
                movieDetailActivity.scrollNestedViewToTop();
            }
        }
    }

    private void fetchCastDetails() {
        final CastDao castDao = MovieDatabase.getInstance(movieDetailActivity).getCastDao();
        final CrewDao crewDao = MovieDatabase.getInstance(movieDetailActivity).getCrewDao();

        if (NetworkUtils.isNetworkConnected(movieDetailActivity)) {
            RetrofitAPI retrofitAPI = NetworkUtils.getRetrofit().create(RetrofitAPI.class);
            Call<CastCrewDetails> castCrewDetailsCall = retrofitAPI.getCredits(mMovie.getId(), BuildConfig.TMDB_API_KEY);
            castCrewDetailsCall.enqueue(new Callback<CastCrewDetails>() {
                @Override
                public void onResponse(Call<CastCrewDetails> call, Response<CastCrewDetails> response) {
                    if (response.body() != null) {
                        CastCrewDetails castCrewDetails = response.body();

                        //get director details
                        if (castCrewDetails.getCrew() != null && castCrewDetails.getCrew().size() > 0) {
                            ArrayList<Crew> allCrew = castCrewDetails.getCrew();

                            for (Crew crew : allCrew) {
                                if (crew.getJob().equals("Director")) {
                                    movieDetailActivity.showDirectorName(crew.getName());
                                    crewDao.insert(new Crew(crew.getId(), crew.getJob(), crew.getName(), mMovie.getId()));
                                    break;
                                }
                            }
                        }

                        //get cast details
                        if (castCrewDetails.getCast() != null && castCrewDetails.getCast().size() > 0) {
                            //show cast list first
                            movieDetailActivity.showCastList(castCrewDetails.getCast());

                            //store then to cast table
                            List<Cast> castList = castCrewDetails.getCast();
                            for (Cast cast : castList) {
                                castDao.insert(new Cast(cast.getId(), cast.getName(), cast.getProfilePath(), mMovie.getId()));
                            }

                        }

                    }
                }

                @Override
                public void onFailure(Call<CastCrewDetails> call, Throwable t) {

                }
            });
        } else {
            List<Crew> crewList = crewDao.findCrewForMovie(mMovie.getId());
            if (crewList != null && crewList.size() > 0) {
                movieDetailActivity.showDirectorName(crewList.get(0).getName());
            }

            List<Cast> castList = castDao.findCastForMovie(mMovie.getId());

            if (castList != null && castList.size() > 0) {
                movieDetailActivity.showCastList((ArrayList<Cast>) castList);
            }
        }

    }
}



