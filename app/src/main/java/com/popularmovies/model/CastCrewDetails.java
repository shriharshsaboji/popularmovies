package com.popularmovies.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created on 23/9/18.
 * shriharsh
 */
public class CastCrewDetails implements Serializable {

    @SerializedName("cast")
    private ArrayList<Cast> cast;

    @SerializedName("crew")
    private ArrayList<Crew> crew;

    public ArrayList<Cast> getCast() {
        return cast;
    }

    public void setCast(ArrayList<Cast> cast) {
        this.cast = cast;
    }

    public ArrayList<Crew> getCrew() {
        return crew;
    }

    public void setCrew(ArrayList<Crew> crew) {
        this.crew = crew;
    }
}
