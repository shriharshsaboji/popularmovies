package com.popularmovies.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created on 23/9/18.
 * shriharsh
 */
@Entity(tableName = "genre", foreignKeys = @ForeignKey(entity = Movie.class,
        parentColumns = "id", childColumns = "movieId", onDelete = ForeignKey.CASCADE))
public class Genre implements Serializable {

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    private int movieId;

    public Genre(String name, int id, int movieId) {
        this.name = name;
        this.id = id;
        this.movieId = movieId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

}
