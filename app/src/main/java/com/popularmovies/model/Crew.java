package com.popularmovies.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created on 23/9/18.
 * shriharsh
 */
@Entity(tableName = "movie_crew",
        primaryKeys = {"id", "movieId"},
        foreignKeys = @ForeignKey(entity = Movie.class,
                parentColumns = "id", childColumns = "movieId", onDelete = ForeignKey.CASCADE))
public class Crew implements Serializable {

    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int id;

    @SerializedName("job")
    private String job;

    @SerializedName("name")
    private String name;

    @ColumnInfo(name = "movieId")
    private int movieId;

    public Crew(int id, String job, String name, int movieId) {
        this.id = id;
        this.job = job;
        this.name = name;
        this.movieId = movieId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
