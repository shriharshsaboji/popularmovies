package com.popularmovies.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created on 23/9/18.
 * shriharsh
 */
@Entity(tableName = "movie_cast",
        primaryKeys = {"id", "movieId"},
        foreignKeys = @ForeignKey(entity = Movie.class,
                parentColumns = "id", childColumns = "movieId", onDelete = ForeignKey.CASCADE))
public class Cast implements Serializable {

    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("profile_path")
    private String profilePath;

    @ColumnInfo(name = "movieId")
    private int movieId;

    public Cast(int id, String name, String profilePath, int movieId) {
        this.id = id;
        this.name = name;
        this.profilePath = profilePath;
        this.movieId = movieId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }
}
