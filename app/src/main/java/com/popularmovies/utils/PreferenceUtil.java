package com.popularmovies.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.Set;

public class PreferenceUtil {

    private static PreferenceUtil mInstance;
    private SharedPreferences mSharedPrefs;
    private Editor mPrefsEditor;

    private PreferenceUtil(final Context context, String name) {
        mSharedPrefs = context.getSharedPreferences(name, Activity.MODE_PRIVATE);
        mPrefsEditor = mSharedPrefs.edit();
    }

    public static void init(final Application application, String preferenceName) {
        if (mInstance == null) {
            mInstance = new PreferenceUtil(application, preferenceName);
        }
    }

    public static PreferenceUtil getInstance() {
        if (mInstance == null) {
            throw new RuntimeException(
                    "Must run init(Application application)");
        }
        return mInstance;
    }

    public void clear() {
        mPrefsEditor.clear();
        mPrefsEditor.commit();
    }

    public void remove(String key) {
        mPrefsEditor.remove(key);
        mPrefsEditor.commit();
    }

    public String getStringValue(final String key, final String defaultvalue) {
        return mSharedPrefs.getString(key, defaultvalue);
    }

    public void setStringValue(final String key, final String value) {
        mPrefsEditor.putString(key, value);
        mPrefsEditor.commit();
    }

    public int getIntValue(final String key, final int defaultvalue) {
        return mSharedPrefs.getInt(key, defaultvalue);
    }

    public void setIntValue(final String key, final int value) {
        mPrefsEditor.putInt(key, value);
        mPrefsEditor.commit();
    }

    public long getLongValue(final String key, final long defaultvalue) {
        return mSharedPrefs.getLong(key, defaultvalue);
    }

    public void setLongValue(final String key, final long value) {
        mPrefsEditor.putLong(key, value);
        mPrefsEditor.commit();
    }

    public boolean getBooleanValue(final String key, final Boolean defaultvalue) {
        return mSharedPrefs.getBoolean(key, defaultvalue);
    }

    public void setBooleanValue(final String key, final boolean value) {
        mPrefsEditor.putBoolean(key, value);
        mPrefsEditor.commit();
    }

    public void putStringSet(String key, Set<String> values) {
        mPrefsEditor.putStringSet(key, values);
        mPrefsEditor.commit();
    }

    public Set<String> getStringset(String key, Set<String> defaultvalue) {
        return mSharedPrefs.getStringSet(key, defaultvalue);
    }
}
