package com.popularmovies.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created on 23/9/18.
 * shriharsh
 */
public class DateUtil {

    public static String changeDateFormat(String releaseDate, String fromPattern, String toPattern) {
        DateFormat sourceDateFormat = new SimpleDateFormat(fromPattern);
        Date date = null;
        try {
            date = sourceDateFormat.parse(releaseDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat destDateFormat = new SimpleDateFormat(toPattern);
        String dateStr = destDateFormat.format(date);
        return dateStr;
    }

}
