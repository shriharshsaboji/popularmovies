package com.popularmovies.network;

import com.popularmovies.model.CastCrewDetails;
import com.popularmovies.model.MovieDetails;
import com.popularmovies.model.MovieListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created on 22/9/18.
 * shriharsh
 */
public interface RetrofitAPI {

    String BASE_URL = "https://api.themoviedb.org/3/";
    String POSTER_BASE_URL = "http://image.tmdb.org/t/p/w342";
    String BACKDROP_BASE_URL = "http://image.tmdb.org/t/p/w500";

    @GET("movie/{type}")
    Call<MovieListResponse> getMovies(@Path("type") String TYPE, @Query("api_key") String API_KEY, @Query("language") String LANGUAGE, @Query("page") int PAGE);

    @GET("movie/{movie_id}")
    Call<MovieDetails> getDetails(@Path("movie_id") int MOVIE_ID, @Query("api_key") String API_KEY, @Query("language") String LANGUAGE);

    @GET("movie/{movie_id}/credits")
    Call<CastCrewDetails> getCredits(@Path("movie_id") int MOVIE_ID, @Query("api_key") String API_KEY);

}
