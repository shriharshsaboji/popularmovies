package com.popularmovies.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.popularmovies.model.Cast;
import com.popularmovies.model.Crew;
import com.popularmovies.model.Genre;
import com.popularmovies.model.Movie;

/**
 * Created on 22/9/18.
 * shriharsh
 */

@Database(entities = {Movie.class, Genre.class, Cast.class, Crew.class}, version = 1)
public abstract class MovieDatabase extends RoomDatabase {

    private static final String DB_NAME = "movie.db";
    private static volatile MovieDatabase instance;

    public static synchronized MovieDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static MovieDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                MovieDatabase.class,
                DB_NAME)
                .allowMainThreadQueries()
                .build();
    }

    public abstract MovieDao getMoviesDao();

    public abstract GenreDao getGenreDao();

    public abstract CastDao getCastDao();

    public abstract CrewDao getCrewDao();

}
