package com.popularmovies.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.popularmovies.model.Cast;
import com.popularmovies.model.Crew;

import java.util.List;

/**
 * Created on 23/9/18.
 * shriharsh
 */
@Dao
public interface CrewDao {

    @Query("SELECT * FROM movie_crew")
    List<Cast> getAllCrew();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Crew> crewList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Crew crew);

    @Update
    void update(Crew... crews);

    @Delete
    void delete(Crew... crews);

    @Query("SELECT * FROM movie_crew WHERE movieId=:movieId")
    List<Crew> findCrewForMovie(final int movieId);

}
