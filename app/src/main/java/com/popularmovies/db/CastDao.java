package com.popularmovies.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.popularmovies.model.Cast;

import java.util.List;

/**
 * Created on 23/9/18.
 * shriharsh
 */
@Dao
public interface CastDao {

    @Query("SELECT * FROM movie_cast")
    List<Cast> getAllCast();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Cast> castList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Cast cast);

    @Update
    void update(Cast... casts);

    @Delete
    void delete(Cast... casts);

    @Query("SELECT * FROM movie_cast WHERE movieId=:movieId")
    List<Cast> findCastForMovie(final int movieId);

}
