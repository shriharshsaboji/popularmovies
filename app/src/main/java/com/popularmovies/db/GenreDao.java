package com.popularmovies.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.popularmovies.model.Genre;

import java.util.List;

/**
 * Created on 23/9/18.
 * shriharsh
 */
@Dao
public interface GenreDao {

    @Query("SELECT * FROM genre")
    List<Genre> getAllGenre();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Genre> genres);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Genre genres);

    @Update
    void update(Genre... genres);

    @Delete
    void delete(Genre... genres);

    @Query("SELECT * FROM genre WHERE movieId=:movieId")
    List<Genre> findGenresForMovie(final int movieId);
}
