package com.popularmovies;

import android.app.Application;

import com.popularmovies.utils.PreferenceUtil;

/**
 * Created on 23/9/18.
 * shriharsh
 */
public class BaseApplication extends Application {

    private static PreferenceUtil mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize a Shared Preference
        PreferenceUtil.init(this, Constants.PREFERENCE_DATABASE_NAME);
    }
}
