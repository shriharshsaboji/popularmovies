package com.popularmovies;

/**
 * Created on 23/9/18.
 * shriharsh
 */
public final class Constants {
    public static final String PREFERENCE_DATABASE_NAME = "movie_prefs";
    public static final String IS_FIRST_LAUNCH = "is_first_launch";
    public static final String EXTRA_KEY_MOVIE = "movie";

}
