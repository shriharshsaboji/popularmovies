# Popular Movies

Popular movies app is the android app which fetches the popular movies from https://www.themoviedb.org/ which is updated on a daily basis.

Commits will be in the below format - 

Chore/Feat/Fix : [x. Relevant message(s)]

Chore - For clean up and refactoring work \
Feat - Feature added \
Fix - Fixed issue

Below are the technology trends used in developing the app

**Language** - Java
**Architecture** - MVP
**Persistence Library** - Room
**UX** - Animations used to please users to use it quite often.
**Others** - Offlinity of the app is maintained, where user  who has us.

**3rd party libraries used** -  Retrofit, Glide, Recyclerview-animators, ChipCloud, ShimmerRecyclerView

**How to test**
Clone the repository and please your developer api key from https://www.themoviedb.org/ and place it in the local.properties file of the cloned project in the below format

tmdb_api_key=XXXXXXXXXXXXX
